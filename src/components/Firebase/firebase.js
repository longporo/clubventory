import {initializeApp} from "firebase/app";
import {
    getAuth,
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    sendPasswordResetEmail,
    signOut
} from 'firebase/auth';
import {getDatabase, ref, onValue, off, push, set, query, update, orderByChild, equalTo} from 'firebase/database';
import {getStorage, ref as stRef, uploadBytes, getDownloadURL} from "firebase/storage";
import {FIREBASE_CLUBS_PATH} from "../../constants/sysUrls";
import * as MyUtils from "../../common/myUtils";
const firebaseConfig = {
    apiKey: "AIzaSyBLCcr9hF1QPK6regEs4Kjy-mos_SWLM_M",
    authDomain: "cs353-team13-6e70d.firebaseapp.com",
    databaseURL: "https://cs353-team13-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "cs353-team13",
    storageBucket: "cs353-team13.appspot.com",
    messagingSenderId: "534665805702",
    appId: "1:534665805702:web:a7626b022ce45b657ff607",
    measurementId: "G-6ZMQLY9S61"
};


const filterUid = (uid) => {
    return function (obj) {
        return obj.uid !== uid;
    }
};

const filterRelatedClub = (uid) => {
    return function (obj) {
        return (obj.create_id !== uid && (!obj.members || !(obj.members[uid])));
    }
};

// Initialize Firebase
class Firebase {
    constructor() {
        const app = initializeApp(firebaseConfig);
        this.auth = getAuth();
        this.db = getDatabase(app);
        this.storage = getStorage(app);
    }

    onValue = onValue;

    doCreateUserWithEmailAndPassword = (email, password) =>
        createUserWithEmailAndPassword(this.auth, email, password);

    doSignInWithEmailAndPassword = (email, password) =>
        signInWithEmailAndPassword(this.auth, email, password);

    doSignOut = () => signOut(this.auth);

    doPasswordReset = email => sendPasswordResetEmail(this.auth, email);

    doPasswordUpdate = password =>
        this.auth.auth.currentUser.updatePassword(password);

    user = (uid, username, email) => set(ref(this.db, `users/${uid}`), {
        username: username,
        email: email
    });

    /**
     * Get clubs by user id.
     * Get clubs which create_id is uid or members include uid.
     * @param uid
     * @returns {Promise<any>}
     */
    getClubsByUid = (uid) => {
        return new Promise((resolve, reject) => {
            new Promise((resolve, reject) => {
                onValue(query(ref(this.db, FIREBASE_CLUBS_PATH), orderByChild("create_id"), equalTo(uid)), (snapshot) => {
                    const result = snapshot.val();
                    let dataList = MyUtils.keyObjToArray(result);
                    resolve(dataList);
                });
            }).then((value) => {
                onValue(query(ref(this.db, FIREBASE_CLUBS_PATH), orderByChild("members/" + uid), equalTo(true)), (snapshot) => {
                    const result = snapshot.val();
                    let dataList = MyUtils.keyObjToArray(result);
                    resolve(value.concat(dataList));
                });
            });
        });
    };

    /**
     * unsubscribe getClubsByUid
     * @param uid
     */
    getClubsByUidOff = (uid) => {
        off(query(ref(this.db, FIREBASE_CLUBS_PATH), orderByChild("create_id"), equalTo(uid)));
        off(query(ref(this.db, FIREBASE_CLUBS_PATH), orderByChild("members/" + uid), equalTo(true)));
    };

    /**
     * Get clubs to join
     * @param uid
     */
    getClubsToJoin = (uid) => {
        return new Promise((resolve, reject) => {
            onValue(ref(this.db, FIREBASE_CLUBS_PATH), (snapshot) => {
                const result = snapshot.val();
                let dataList = MyUtils.keyObjToArray(result).filter(filterRelatedClub(uid));
                resolve(dataList);
            });
        });
    };

    /**
     * Join club by uid
     * @param clubId
     * @param uid
     * @returns {Promise<void>}
     */
    joinClubByUid = (clubId, uid) => {
        const updates = {};
        updates[FIREBASE_CLUBS_PATH + clubId + "/members/" + uid] = true;
        return update(ref(this.db), updates);
    };

    /**
     * Exit club.
     * @param clubId
     * @param uid
     * @returns {Promise<void>}
     */
    exitClub = (clubId, uid) => {
        const updates = {};
        updates[FIREBASE_CLUBS_PATH + clubId + "/members/" + uid] = null;
        return update(ref(this.db), updates);
    };

    /**
     * Delete club.
     * @param clubId
     * @returns {Promise<void>}
     */
    delClub = (clubId) => {
        const updates = {};
        updates[FIREBASE_CLUBS_PATH + clubId] = null;
        return update(ref(this.db), updates);
    };

    /**
     * insert data to firebase by path.
     * @param data
     * @param path
     * @returns {Promise<any>|Promise<void>}
     */
    insert = (data, path) => {
        if (!path) {
            return new Promise((resolve, reject)=>{
                reject("insert path missing...");
            });
        }
        return set(push(ref(this.db, path)), data)
    };

    /**
     * Get user list.
     * @param currUid
     * @returns {Promise<any>}
     */
    users = (currUid) => new Promise((resolve, reject) => {
        onValue(ref(this.db, 'users'), (snapshot) => {
            const result = snapshot.val();
            let dataList = MyUtils.keyObjToArray(result, "uid").filter(filterUid(currUid));
            resolve(dataList);
        });
    });

    /**
     * Off users listening.
     */
    offUsers = () => off(ref(this.db, 'users'));

    /**
     * Upload file
     * @param file
     * @param path
     * @returns {Promise<UploadResult>}
     */
    uploadFile = (file, path) => {
        let storageRef = stRef(this.storage, path);
        return uploadBytes(storageRef, file);
    };

    /**
     * Get download url by storage ref.
     * @param storageRef
     * @returns {Promise<string>}
     */
    getDownloadURL = (storageRef) => {
        return getDownloadURL(storageRef);
    };
}

export default Firebase;