import React, {useContext, useEffect} from "react";
import {compose} from "recompose";
import {FirebaseContext, withFirebase} from "../Firebase";
import {withAuthorization} from "../Session";
import Box from "@material-ui/core/Box";
import {styled} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import AuthUserContext from "../Session/context";
import Avatar from "@material-ui/core/Avatar";
import Switch from "@material-ui/core/Switch";
import ListItemAvatar from "@material-ui/core/ListItemAvatar/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import ListItem from "@material-ui/core/ListItem";

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(1),
    width: '88%',
    marginLeft: '6%',
    textAlign: 'center',
    bgcolor: 'background.paper',
    color: theme.palette.text.secondary,
}));

const SubItem = styled(Card)(({ theme }) => ({
    ...theme.typography.body2,
    margin: 20,
    display: "inline-block",
    width: 220,
    color: theme.palette.text.secondary,
}));

function MyItemsPage() {

    // firebase context
    const firebase = useContext(FirebaseContext);

    // currUser
    const currUser = useContext(AuthUserContext);

    // get user list from firebase
    const [userList, setUserList] = React.useState([]);
    useEffect(() => {
        if (userList.length > 0) {
            return;
        }
        firebase.users(currUser.uid).then((value) => {
            setUserList(value);
        });
        return () => {
            firebase.offUsers();
        }
    });

    return (
        <Box m={4}>
            <Grid container spacing={2}>
                <Item>
                    <Typography gutterBottom variant="h4" component="div" sx={{textAlign: "left"}}>
                        My borrowed items
                    </Typography>
                    <SubItem>
                        <CardMedia
                            component="img"
                            alt="green iguana"
                            height="180"
                            image="https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2Fef3357-c27-5e30-e52f-a216ce203-luna.png?alt=media&token=d6ba4c4e-a386-4f55-8a35-e601a9aeea9b"
                        />
                        <Typography gutterBottom variant="h6" component="div" sx={{color: "#e57373", marginBottom: "0px"}}>
                            Item Name
                        </Typography>
                        <CardActions sx={{paddingTop: "0px"}}>
                            <ListItem sx={{paddingLeft: "0px !important", paddingRight: "0px !important"}}>
                                <ListItemAvatar>
                                    <Avatar
                                        src={"https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2F4266c8-3b10-08e-63a-bb0b28a5baf-logo.png?alt=media&token=94c1fec9-1db2-47f5-afca-8f4d0228d4dd"}
                                    />
                                </ListItemAvatar>
                                <ListItemText primary={"Return"}
                                              style={{textAlign: 'center'}}/>
                                <Switch
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            </ListItem>
                        </CardActions>
                    </SubItem>
                    <SubItem>
                        <CardMedia
                            component="img"
                            alt="green iguana"
                            height="180"
                            image="https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2Fef3357-c27-5e30-e52f-a216ce203-luna.png?alt=media&token=d6ba4c4e-a386-4f55-8a35-e601a9aeea9b"
                        />
                        <Typography gutterBottom variant="h6" component="div" sx={{color: "#e57373", marginBottom: "0px"}}>
                            Item Name
                        </Typography>
                        <CardActions sx={{paddingTop: "0px"}}>
                            <ListItem sx={{paddingLeft: "0px !important", paddingRight: "0px !important"}}>
                                <ListItemAvatar>
                                    <Avatar
                                        src={"https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2F4266c8-3b10-08e-63a-bb0b28a5baf-logo.png?alt=media&token=94c1fec9-1db2-47f5-afca-8f4d0228d4dd"}
                                    />
                                </ListItemAvatar>
                                <ListItemText primary={"Return"}
                                              style={{textAlign: 'center'}}/>
                                <Switch
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            </ListItem>
                        </CardActions>
                    </SubItem>
                    <SubItem>
                        <CardMedia
                            component="img"
                            alt="green iguana"
                            height="180"
                            image="https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2Fef3357-c27-5e30-e52f-a216ce203-luna.png?alt=media&token=d6ba4c4e-a386-4f55-8a35-e601a9aeea9b"
                        />
                        <Typography gutterBottom variant="h6" component="div" sx={{color: "#e57373", marginBottom: "0px"}}>
                            Item Name
                        </Typography>
                        <CardActions sx={{paddingTop: "0px"}}>
                            <ListItem sx={{paddingLeft: "0px !important", paddingRight: "0px !important"}}>
                                <ListItemAvatar>
                                    <Avatar
                                        src={"https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2F4266c8-3b10-08e-63a-bb0b28a5baf-logo.png?alt=media&token=94c1fec9-1db2-47f5-afca-8f4d0228d4dd"}
                                    />
                                </ListItemAvatar>
                                <ListItemText primary={"Return"}
                                              style={{textAlign: 'center'}}/>
                                <Switch
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            </ListItem>
                        </CardActions>
                    </SubItem>
                    <SubItem>
                        <CardMedia
                            component="img"
                            alt="green iguana"
                            height="180"
                            image="https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2Fef3357-c27-5e30-e52f-a216ce203-luna.png?alt=media&token=d6ba4c4e-a386-4f55-8a35-e601a9aeea9b"
                        />
                        <Typography gutterBottom variant="h6" component="div" sx={{color: "#e57373", marginBottom: "0px"}}>
                            Item Name
                        </Typography>
                        <CardActions sx={{paddingTop: "0px"}}>
                            <ListItem sx={{paddingLeft: "0px !important", paddingRight: "0px !important"}}>
                                <ListItemAvatar>
                                    <Avatar
                                        src={"https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2F4266c8-3b10-08e-63a-bb0b28a5baf-logo.png?alt=media&token=94c1fec9-1db2-47f5-afca-8f4d0228d4dd"}
                                    />
                                </ListItemAvatar>
                                <ListItemText primary={"Return"}
                                              style={{textAlign: 'center'}}/>
                                <Switch
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            </ListItem>
                        </CardActions>
                    </SubItem>
                    <SubItem>
                        <CardMedia
                            component="img"
                            alt="green iguana"
                            height="180"
                            image="https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2Fef3357-c27-5e30-e52f-a216ce203-luna.png?alt=media&token=d6ba4c4e-a386-4f55-8a35-e601a9aeea9b"
                        />
                        <Typography gutterBottom variant="h6" component="div" sx={{color: "#e57373", marginBottom: "0px"}}>
                            Item Name
                        </Typography>
                        <CardActions sx={{paddingTop: "0px"}}>
                            <ListItem sx={{paddingLeft: "0px !important", paddingRight: "0px !important"}}>
                                <ListItemAvatar>
                                    <Avatar
                                        src={"https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2F4266c8-3b10-08e-63a-bb0b28a5baf-logo.png?alt=media&token=94c1fec9-1db2-47f5-afca-8f4d0228d4dd"}
                                    />
                                </ListItemAvatar>
                                <ListItemText primary={"Return"}
                                              style={{textAlign: 'center'}}/>
                                <Switch
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            </ListItem>
                        </CardActions>
                    </SubItem>
                    <SubItem>
                        <CardMedia
                            component="img"
                            alt="green iguana"
                            height="180"
                            image="https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2Fef3357-c27-5e30-e52f-a216ce203-luna.png?alt=media&token=d6ba4c4e-a386-4f55-8a35-e601a9aeea9b"
                        />
                        <Typography gutterBottom variant="h6" component="div" sx={{color: "#e57373", marginBottom: "0px"}}>
                            Item Name
                        </Typography>
                        <CardActions sx={{paddingTop: "0px"}}>
                            <ListItem sx={{paddingLeft: "0px !important", paddingRight: "0px !important"}}>
                                <ListItemAvatar>
                                    <Avatar
                                        src={"https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2F4266c8-3b10-08e-63a-bb0b28a5baf-logo.png?alt=media&token=94c1fec9-1db2-47f5-afca-8f4d0228d4dd"}
                                    />
                                </ListItemAvatar>
                                <ListItemText primary={"Return"}
                                              style={{textAlign: 'center'}}/>
                                <Switch
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            </ListItem>
                        </CardActions>
                    </SubItem>
                    <SubItem>
                        <CardMedia
                            component="img"
                            alt="green iguana"
                            height="180"
                            image="https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2Fef3357-c27-5e30-e52f-a216ce203-luna.png?alt=media&token=d6ba4c4e-a386-4f55-8a35-e601a9aeea9b"
                        />
                        <Typography gutterBottom variant="h6" component="div" sx={{color: "#e57373", marginBottom: "0px"}}>
                            Item Name
                        </Typography>
                        <CardActions sx={{paddingTop: "0px"}}>
                            <ListItem sx={{paddingLeft: "0px !important", paddingRight: "0px !important"}}>
                                <ListItemAvatar>
                                    <Avatar
                                        src={"https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2F4266c8-3b10-08e-63a-bb0b28a5baf-logo.png?alt=media&token=94c1fec9-1db2-47f5-afca-8f4d0228d4dd"}
                                    />
                                </ListItemAvatar>
                                <ListItemText primary={"Return"}
                                              style={{textAlign: 'center'}}/>
                                <Switch
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            </ListItem>
                        </CardActions>
                    </SubItem>
                    <SubItem>
                        <CardMedia
                            component="img"
                            alt="green iguana"
                            height="180"
                            image="https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2Fef3357-c27-5e30-e52f-a216ce203-luna.png?alt=media&token=d6ba4c4e-a386-4f55-8a35-e601a9aeea9b"
                        />
                        <Typography gutterBottom variant="h6" component="div" sx={{color: "#e57373", marginBottom: "0px"}}>
                            Item Name
                        </Typography>
                        <CardActions sx={{paddingTop: "0px"}}>
                            <ListItem sx={{paddingLeft: "0px !important", paddingRight: "0px !important"}}>
                                <ListItemAvatar>
                                    <Avatar
                                        src={"https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2F4266c8-3b10-08e-63a-bb0b28a5baf-logo.png?alt=media&token=94c1fec9-1db2-47f5-afca-8f4d0228d4dd"}
                                    />
                                </ListItemAvatar>
                                <ListItemText primary={"Return"}
                                              style={{textAlign: 'center'}}/>
                                <Switch
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            </ListItem>
                        </CardActions>
                    </SubItem>
                    <SubItem>
                        <CardMedia
                            component="img"
                            alt="green iguana"
                            height="180"
                            image="https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2Fef3357-c27-5e30-e52f-a216ce203-luna.png?alt=media&token=d6ba4c4e-a386-4f55-8a35-e601a9aeea9b"
                        />
                        <Typography gutterBottom variant="h6" component="div" sx={{color: "#e57373", marginBottom: "0px"}}>
                            Item Name
                        </Typography>
                        <CardActions sx={{paddingTop: "0px"}}>
                            <ListItem sx={{paddingLeft: "0px !important", paddingRight: "0px !important"}}>
                                <ListItemAvatar>
                                    <Avatar
                                        src={"https://firebasestorage.googleapis.com/v0/b/cs353-team13.appspot.com/o/club-img%2F4266c8-3b10-08e-63a-bb0b28a5baf-logo.png?alt=media&token=94c1fec9-1db2-47f5-afca-8f4d0228d4dd"}
                                    />
                                </ListItemAvatar>
                                <ListItemText primary={"Return"}
                                              style={{textAlign: 'center'}}/>
                                <Switch
                                    inputProps={{ 'aria-label': 'controlled' }}
                                />
                            </ListItem>
                        </CardActions>
                    </SubItem>
                </Item>
            </Grid>
        </Box>
    );
}
const condition = authUser => !!authUser;
export default compose(withFirebase, withAuthorization(condition))(MyItemsPage);