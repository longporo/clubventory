import * as React from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from "recompose";
import {styled, alpha} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';

import logo from "../../img/logo/logo.png";
import * as ROUTES from "../../constants/routes";
import {withFirebase} from "../Firebase";
import Avatar from "@material-ui/core/Avatar";
import {withAuthUser} from "../Session";

class Navigation extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            anchorEl: null,
            mobileMoreAnchorEl: null,
            loaded: false,
        };
    }

    componentDidMount () {
    }


    componentDidUpdate () {
    }


    render() {
        const Search = styled('div')(({theme}) => ({
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: alpha(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: alpha(theme.palette.common.white, 0.25),
            },
            marginRight: theme.spacing(2),
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(3),
                width: 'auto',
            },
        }));

        const SearchIconWrapper = styled('div')(({theme}) => ({
            padding: theme.spacing(0, 2),
            height: '100%',
            position: 'absolute',
            pointerEvents: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        }));

        const StyledInputBase = styled(InputBase)(({theme}) => ({
            color: 'inherit',
            '& .MuiInputBase-input': {
                padding: theme.spacing(1, 1, 1, 0),
                // vertical padding + font size from searchIcon
                paddingLeft: `calc(1em + ${theme.spacing(4)})`,
                transition: theme.transitions.create('width'),
                width: '100%',
                [theme.breakpoints.up('md')]: {
                    width: '40ch',
                },
            },
        }));

        const isMenuOpen = Boolean(this.state.anchorEl);
        const isMobileMenuOpen = Boolean(this.state.mobileMoreAnchorEl);

        const handleLogoClick = (event) => {
            this.props.history.push(ROUTES.LANDING);
        };

        const handleProfileMenuOpen = (event) => {
            this.setState({anchorEl: event.currentTarget})
        };

        const handleMobileMenuClose = () => {
            this.setState({mobileMoreAnchorEl: null})
        };

        const handleMenuClose = () => {
            this.setState({anchorEl: null});
            handleMobileMenuClose();
        };

        const handleAccountClick = () => {
            handleMenuClose();
            this.props.history.push(ROUTES.ACCOUNT);
        };

        const handleSignOutClick = () => {
            handleMenuClose();
            this.props.firebase.doSignOut();
        };

        const menuId = 'primary-search-account-menu';
        const renderMenu = (
            <Menu
                anchorEl={this.state.anchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                id={menuId}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={isMenuOpen}
                onClose={handleMenuClose}
            >
                <MenuItem onClick={handleAccountClick}>My account</MenuItem>
                <MenuItem onClick={handleSignOutClick}>Sign Out</MenuItem>
            </Menu>
        );

        const mobileMenuId = 'primary-search-account-menu-mobile';
        const renderMobileMenu = (
            <Menu
                anchorEl={this.state.mobileMoreAnchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                id={mobileMenuId}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={isMobileMenuOpen}
                onClose={handleMobileMenuClose}
            >
                <MenuItem onClick={handleProfileMenuOpen}>
                    <IconButton
                        size="large"
                        aria-label="account of current user"
                        aria-controls="primary-search-account-menu"
                        aria-haspopup="true"
                        color="inherit"
                    >
                        <AccountCircle/>
                    </IconButton>
                    <p>Sign Out</p>
                </MenuItem>
            </Menu>
        );
        let helloWords = null;
        if (this.props.authUser) {
            helloWords = "Hello, " + this.props.authUser.email
        } else {
            return null;
        }
        return (
            <Box sx={{flexGrow: 1}}>
                <AppBar position="static">
                    <Toolbar>
                        <Box sx={{display: {xs: 'none', md: 'flex'}}}>
                            <IconButton
                                size="large"
                                edge="end"
                                aria-label="account of current user"
                                aria-controls={menuId}
                                aria-haspopup="true"
                                onClick={handleLogoClick}
                                color="inherit"
                            >
                                <Avatar
                                    src={logo}
                                />
                            </IconButton>
                        </Box>
                        <Box sx={{display: {xs: 'none', md: 'flex'}}}>
                            <h3>&nbsp;&nbsp;&nbsp;Clubventory!</h3>
                        </Box>
                        <Search>
                            <SearchIconWrapper>
                                <SearchIcon/>
                            </SearchIconWrapper>
                            <StyledInputBase
                                placeholder="Search…"
                                inputProps={{'aria-label': 'search'}}
                            />
                        </Search>
                        <Box sx={{flexGrow: 1}}/>
                        <Box sx={{display: {xs: 'none', md: 'flex'}}}>
                            <h5>{helloWords}</h5>
                        </Box>
                        <Box sx={{display: {xs: 'none', md: 'flex'}}}>
                            <IconButton
                                size="large"
                                edge="end"
                                aria-label="account of current user"
                                aria-controls={menuId}
                                aria-haspopup="true"
                                onClick={handleProfileMenuOpen}
                                color="inherit"
                            >
                                <AccountCircle/>
                            </IconButton>
                        </Box>
                    </Toolbar>
                </AppBar>
                {renderMobileMenu}
                {renderMenu}
            </Box>
        );
    }
}
export default compose(withFirebase, withRouter, withAuthUser)(Navigation);