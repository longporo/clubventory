export const LANDING = '/';
export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const CLUB_ADD = '/club/add';
export const MY_ITEMS = '/my/items';
export const ACCOUNT = '/account';
export const PASSWORD_FORGET = '/pw-forget';